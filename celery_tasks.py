import ast
import asyncio
import logging
import logging.handlers
import os
import redis_lock

from celery import Celery
from celery.signals import task_prerun, worker_ready
from celery.utils.log import get_task_logger
from pymongo import MongoClient

from . import sync_tasks, async_tasks
from . import celeryconfig, config


app = Celery()
app.config_from_object(celeryconfig)
logger = get_task_logger(__name__)

client = MongoClient(host=config.MONGO_CONFIG['host'],
                     port=config.MONGO_CONFIG['port'],
                     connect=config.MONGO_CONFIG['connect'])
db = client[config.MONGO_CONFIG['db']]

clients_manager_lock = redis_lock.Lock(async_tasks.redis_conn, "clients_manager_lock")
clients_manager_lock.reset()

clients_runner_lock = redis_lock.Lock(async_tasks.redis_conn, "clients_runner_lock")
clients_runner_lock.reset()


def _mongo_profiles(db):
    return db.worker.find({'state': {'$nin': ['banned', 'disabled']}})


def _ranked_profiles(db):
    profiles = {}

    for profile in _mongo_profiles(db):
        profile_chats_count = db.channel.count({'worker': profile['phone']})

        if profile_chats_count < config.CHANNELS_PER_PROFILE:
            profiles[profile['phone']] = profile_chats_count

    return profiles


@app.task(bind=True, queue='tg_queue')
def manage_new_channels(self):
    channels = db.channel.find({'status': {'$nin': ['pending', 'error', 'success', 'ban']}})

    if channels:
        profiles = _ranked_profiles(db)

        for channel in channels:
            if profiles:
                preferred_profile = min(profiles.keys(), key=lambda x: profiles[x])

                if not channel['is_private']:
                    sync_tasks.join_chat(preferred_profile, channel['username'])
                else:
                    sync_tasks.join_chat(preferred_profile, channel['join_hash'])

                profiles[preferred_profile] += 1
                if profiles[preferred_profile] >= config.CHANNELS_PER_PROFILE:
                    profiles.pop(preferred_profile)

                logger.info(f'Assigned channel {channel["username"]} to {preferred_profile}')
            else:
                logger.warning(f'No free profiles left. Skipping channel {channel}...')


@app.task(bind=True, queue='tg_queue')
def manage_channel_bans(self):
    channels = db.telegram_channel.find({'status': 'ban'})

    if channels:
        profiles = _ranked_profiles(db)

        for channel in channels:
            free_profiles = [x for x in profiles if x not in channel.get('ban_history', [])]

            if free_profiles:
                preferred_profile = min(free_profiles, key=lambda x: profiles[x])

                if not channel['is_private']:
                    sync_tasks.join_chat(preferred_profile, channel['username'])
                else:
                    sync_tasks.join_chat(preferred_profile, channel['join_hash'])

                profiles[preferred_profile] += 1
                if profiles[preferred_profile] >= config.CHANNELS_PER_PROFILE:
                    profiles.pop(preferred_profile)

                logger.info(f'Assigned banned channel {channel["username"]} to {preferred_profile}')

            else:
                logger.warning(f'No free profiles left. Skipping channel {channel}...')


@app.task(bind=True, queue='tg_queue')
def manage_profiles(self):
    lock = redis_lock.Lock(async_tasks.redis_conn, "clients_manager_lock")
    if lock.acquire(blocking=False):
        i = self.app.control.inspect()

        worker_task = None
        running_clients = []
        active_tasks = i.active()
        for hostname in active_tasks:
            for task in active_tasks[hostname]:
                if task['name'] == start_clients.name:
                    worker_task = task
                    kwargs = ast.literal_eval(task['kwargs'])
                    running_clients = kwargs['profiles']

        mongo_profiles = [{'phone': profile['phone']} for profile in _mongo_profiles(db)]
        if not mongo_profiles:
            lock.release()
            return
        mongo_profiles.sort(key=lambda x: x['phone'])

        if not worker_task:
            logger.warning(f'Starting asyncio pool with profiles: {mongo_profiles}')
            start_clients.apply_async(kwargs={'profiles': mongo_profiles})

        else:
            running_clients.sort(key=lambda x: x['phone'])
            if mongo_profiles != running_clients:
                logger.warning('Profiles change detected. Restarting asyncio pool...')
                logger.warning(f'Running profiles: {running_clients}')
                logger.warning(f'Mongo profiles: {mongo_profiles}')
                logger.warning("Terminating asyncio pool...")

                self.app.control.revoke(worker_task['id'], terminate=True, reply=True)

                start_clients.apply_async(kwargs={'profiles': mongo_profiles})
            else:
                logger.warning('No profiles change detected.')

        lock.release()
    else:
        logger.warning('Another clients-manager is running.')


@app.task(bind=True, queue='tg_queue')
def start_clients(self, profiles=None):
    if not profiles:
        return

    lock = redis_lock.Lock(async_tasks.redis_conn, "clients_runner_lock")

    if lock.acquire(blocking=False):
        loop = asyncio.get_event_loop()

        for profile in profiles:
            loop.create_task(async_tasks.run_client(profile['phone']))

        try:
            loop.run_forever()
        finally:
            loop.close()
            lock.release()
    else:
        logger.warning('Failed to start loop, another task is running.')


# prepare custom logger for your task
@task_prerun.connect()
def prepare_logging(task=None, *args, **kwargs):
    if task.name == 'feed_bot.feed_bot_worker.celery_tasks.start_clients':
        logger = logging.getLogger()

        logger.handlers = []

        work_dir = os.getcwd()
        fh = logging.handlers.RotatingFileHandler(os.path.join(work_dir, 'clients_logs.log'),
                                                  maxBytes=100000000,
                                                  backupCount=5)
        formatter = logging.Formatter('[%(asctime)s: %(levelname)s/%(processName)s] - %(name)s - %(message)s')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)


# tasks on celery startup
@worker_ready.connect
def at_start(sender, **kwargs):
    with sender.app.connection() as conn:
        sender.app.send_task('socionet.telegram_scraper.celery_tasks.manage_profiles', connection=conn)
