#!/bin/bash
ps -ef | grep 'telegram_scraper' | grep -v grep | awk '{print $2}' | xargs -r kill -9
celery worker -A feed_bot.feed_bot_worker.celery_tasks -B -c 2 --loglevel=DEBUG -f telegram_scraper.log -n telegram_scraper