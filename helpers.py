import logging

from hotqueue import HotQueue
from redis import Redis

try:
    import cPickle as pickle
except ImportError:
    import pickle

logger = logging.getLogger()


class HotQueue(HotQueue):
    def __init__(self, name, serializer=pickle, connection=None, **kwargs):
        self.name = name
        self.serializer = serializer
        if connection:
            self.__redis = connection
        else:
            self.__redis = Redis(**kwargs)

    @property
    def key(self):
        """Return the key name used to store this queue in Redis."""
        return self.name

    def get_all(self):
        msgs = self.__redis.lrange(self.key, 0, -1)
        if msgs and self.serializer:
            return [self.serializer.loads(msg) for msg in msgs]
        else:
            return msgs


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
