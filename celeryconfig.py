from kombu import Queue
from datetime import timedelta


broker_url = 'amqp://localhost:5672//'
result_backend = 'rpc://'
task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Europe/Kiev'
include = ['feed_bot.feed_bot_worker.celery_tasks']


task_queues = (
    Queue('tg_queue', routing_key='tg_queue'),
)


task_routes = {
    'feed_bot.feed_bot_worker.celery_tasks.manage_profiles': {'queue': 'tg_queue'},
    'feed_bot.feed_bot_worker.celery_tasks.start_clients': {'queue': 'tg_queue'},
    'feed_bot.feed_bot_worker.celery_tasks.manage_new_channels': {'queue': 'tg_queue'},
    'feed_bot.feed_bot_worker.celery_tasks.manage_channel_bans': {'queue': 'tg_queue'}
}


beat_schedule = {
    'feed_bot.telegram_manage_profiles_schedule': {
        'task': 'feed_bot.feed_bot_worker.celery_tasks.manage_profiles',
        'schedule': timedelta(seconds=20),
        'options': {
            'queue': 'tg_queue',
            'routing_key': 'tg_queue'
        }
    },
    'telegram_manage_new_channels_schedule': {
        'task': 'feed_bot.feed_bot_worker.celery_tasks.manage_new_channels',
        'schedule': timedelta(seconds=30),
        'options': {
            'queue': 'tg_queue',
            'routing_key': 'tg_queue'
        }
    },
    'telegram_check_channels_bans_schedule': {
        'task': 'feed_bot.feed_bot_worker.celery_tasks.manage_channel_bans',
        'schedule': timedelta(seconds=40),
        'options': {
            'queue': 'tg_queue',
            'routing_key': 'tg_queue'
        }
    }
}

beat_schedule_filename = 'celerybeat-schedule.feed_bot_worker'
