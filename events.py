from telethon import events
from telethon.errors import UnauthorizedError
from telethon.tl.types import Channel, User, Message, MessageService, Chat, ChannelForbidden


@events.register(events.NewMessage())
async def on_message(event):
    try:
        client = event.client

        if not event.chat:
            await event.get_chat()

        if event.is_channel or event.is_group:

            if event.message.from_id:
                sender = await event.get_sender()
            else:
                sender = event.chat

            if isinstance(event.chat, ChannelForbidden):
                client._banned(event.chat.id)

            if isinstance(event.message, MessageService):
                return

            try:
                assert isinstance(event.message, Message)
                assert isinstance(event.chat, (Channel, Chat))
                assert isinstance(sender, (Channel, User))
            except AssertionError:
                client.logger.error("AssertionError")
                client.logger.info(f'sender: {sender}')
                client.logger.info(f'message: {event.message}')
                client.logger.info(f'chat: {event.chat}')
                return

            await client.forward_messages(client.bot_chat, event.message)

            client.update_last_msg(event.chat.id, event.message.id)
            await client.send_read_acknowledge(event.chat, event.message)

            client.logger.info(f'New message: {event.chat.id}_{event.message.id} ')

        else:
            client.logger.info(f'Passing message: {event._message_id} from chat_id: {event.chat.id} (not from group/channel)')

        # ping - pong
        if event.message.raw_text == 'pata':
            sender = await event.get_sender()
            await client.send_message(sender, 'pon')
    except UnauthorizedError as e:
        event.client.logger.error(str(e))

        if any(msg in e.message for msg in ['USER_DEACTIVATED_BAN', 'PHONE_NUMBER_BANNED']):
            event.client.set_mongo_banned()
        else:
            event.client.set_mongo_disabled()

        event.client.disconnect()
